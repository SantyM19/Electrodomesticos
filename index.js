//  import  CONSUMO  from './Models/ValueObjects/Consumo'
//  import { PROCEDENCIA as Procedencia} from './Models/ValueObjects/Procedencia'
//  import { Electrodomestico } from "./Models/Electrodomestico"

const consumo = require("./Models/ValueObjects/Consumo");
const procedencia = require("./Models/ValueObjects/Procedencia");
const Electrodomestico = require("./Models/Electrodomestico");
const Nevera = require("./Models/Nevera");
const Tv = require("./Models/Tv");


var electrodomestico = new Electrodomestico(consumo.A, procedencia.Importado)
var nevera = new Nevera(consumo.B, procedencia.Nacional, 130)
var tv = new Tv(consumo.C, procedencia.Nacional, true, 42)


var preguntas = ['Cuál es tu nombre?',
                  'Cuántos años tienes?',
                'Lenguaje de programación favorito?'];

var respuestas = [];

function pregunta(i){
    process.stdout.write(preguntas[i]);
}

process.stdin.on('data', function(data){
    respuestas.push(data.toString().trim());

    if(respuestas.length < preguntas.length){
        pregunta(respuestas.length);
    }else{
        process.exit();
    }
});

pregunta(0);