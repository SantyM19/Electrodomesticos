const Electrodomestico = require("./Electrodomestico");

class TV extends Electrodomestico {
    constructor(consumo , procedencia, tdt, tamaño) {
      super(consumo , procedencia)
      this.tdt = tdt
      this.tamaño = tamaño
      this.setNewTotal( this.tdt, this.tamaño, this.getTotal);
    }

    setNewTotal( tdt, tamaño, total){
        let tot = (tamaño > 40) ? total += total * 0.3 : total ;
        this.total = (tdt) ? tot += 250000 : tot ;
    }
  }

  module.exports = TV;