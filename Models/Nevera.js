const Electrodomestico = require("./Electrodomestico");

class Nevera extends Electrodomestico {
    constructor(consumo , procedencia, capacidad) {
      super(consumo , procedencia)
      this.capacidad = capacidad
      this.setNewTotal( this.capacidad, this.getTotal);
    }

    setNewTotal( capacidad, total){
        let more = Math.round((capacidad - 120.0)/10)
        
        this.total = (capacidad > 120.0) ? total += total * 0.05 * more : total
    }
  }

  module.exports = Nevera;