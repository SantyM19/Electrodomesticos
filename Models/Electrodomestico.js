class Electrodomestico{

    constructor(consumo , procedencia){
        this.consumoPrecio = consumo * 1.0
        this.procedenciaPrecio = procedencia * 1.0
        this.total = this.consumoPrecio + this.procedenciaPrecio
    }

    get getConsumoPrecio(){
        return this.consumoPrecio
    }

    get getProcedenciaPrecio(){
        return this.procedenciaPrecio
    }

    get getTotal(){
        return this.total
    }

    set setTotal(total){
        this.total = total 
    }

}

// export default Electrodomestico;
module.exports = Electrodomestico;